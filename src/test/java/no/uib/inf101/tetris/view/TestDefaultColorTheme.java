package no.uib.inf101.tetris.view;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import java.awt.Color;
import org.junit.jupiter.api.Test;

public class TestDefaultColorTheme {
    
    @Test
public void sanityTestDefaultColorTheme() {
  ColorTheme colors = new DefaultColorTheme();
  assertEquals(null, colors.getBackgroundColor());
  assertEquals(new Color(0, 0, 0, 0), colors.getFrameColor());
  assertEquals(Color.BLACK, colors.getCellColor('-'));
  assertEquals(Color.RED, colors.getCellColor('L'));
  assertEquals(Color.CYAN, colors.getCellColor('T'));
  assertEquals(Color.ORANGE, colors.getCellColor('O'));
  assertEquals(Color.BLUE, colors.getCellColor('P'));
  assertEquals(Color.GREEN, colors.getCellColor('S'));
  assertEquals(Color.YELLOW, colors.getCellColor('I'));
  assertEquals(Color.PINK, colors.getCellColor('J'));
  assertEquals(Color.MAGENTA, colors.getCellColor('Z'));
  assertThrows(IllegalArgumentException.class, () -> colors.getCellColor('\n'));
}

}
