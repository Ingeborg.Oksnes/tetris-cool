package no.uib.inf101.tetris.model.tetromino;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import no.uib.inf101.grid.CellPosition;
import no.uib.inf101.grid.GridCell;

import no.uib.inf101.tetris.model.TetrisBoard;

public class TestTetromino {

@Test
public void testHashCodeAndEquals() {
  Tetromino t1 = Tetromino.newTetromino('T');
  Tetromino t2 = Tetromino.newTetromino('T');
  Tetromino t3 = Tetromino.newTetromino('T').shiftedBy(1, 0);
  Tetromino s1 = Tetromino.newTetromino('S');
  Tetromino s2 = Tetromino.newTetromino('S').shiftedBy(0, 0);

  assertEquals(t1, t2);
  assertEquals(s1, s2);
  assertEquals(t1.hashCode(), t2.hashCode());
  assertEquals(s1.hashCode(), s2.hashCode());
  assertNotEquals(t1, t3);
  assertNotEquals(t1, s1);
}

@Test
public void tetrominoIterationOfT() {
  // Create a standard 'T' tetromino placed at (10, 100) to test
  Tetromino tetro = Tetromino.newTetromino('T');
  tetro = tetro.shiftedBy(10, 100);

  // Collect which objects are iterated through
  List<GridCell<Character>> objs = new ArrayList<>();
  for (GridCell<Character> gc : tetro) {
    objs.add(gc);
  }

  // Check that we got the expected GridCell objects
  assertEquals(4, objs.size());
  assertTrue(objs.contains(new GridCell<>(new CellPosition(11, 100), 'T')));
  assertTrue(objs.contains(new GridCell<>(new CellPosition(11, 101), 'T')));
  assertTrue(objs.contains(new GridCell<>(new CellPosition(11, 102), 'T')));
  assertTrue(objs.contains(new GridCell<>(new CellPosition(12, 101), 'T')));
}

@Test
public void tetrominoIterationOfS() {
  //lager en "S" tetromino plassert i (10, 100)
  Tetromino tetro = Tetromino.newTetromino('S');
  tetro = tetro.shiftedBy(10, 100);

  // Collect which objects are iterated through
  List<GridCell<Character>> objs = new ArrayList<>();
  for (GridCell<Character> gc : tetro) {
    objs.add(gc);
  }
   // Check that we got the expected GridCell objects
   assertEquals(4, objs.size());
   assertTrue(objs.contains(new GridCell<>(new CellPosition(11, 101), 'S')));
   assertTrue(objs.contains(new GridCell<>(new CellPosition(11, 102), 'S')));
   assertTrue(objs.contains(new GridCell<>(new CellPosition(12, 101), 'S')));
   assertTrue(objs.contains(new GridCell<>(new CellPosition(12, 100), 'S')));


}

@Test
public void testShiftedBy() { //tester shiftedby, og at hvis du flytter brikken to ganger like langt så vil du ha flyttet dobbelt så langt som hvis du flyttet brikken én gang
    Tetromino tetromino = Tetromino.newTetromino('I'); //oppretter en ny tetromino av typen "I"
    tetromino = tetromino.shiftedBy(0, 0); //setter den ytil posisjonen (0,0)

    int deltaRow = 1; //definerer variabler for å forskyve Tetromino-objektet.
    int deltaCol = 2;

    Tetromino shiftedOnce = tetromino.shiftedBy(deltaRow, deltaCol); //forskyver tetromino-objektet en gang med deltaRow og deltaCol
    Tetromino shiftedTwice = shiftedOnce.shiftedBy(deltaRow, deltaCol); //forskyver tetromino-objektet en gang til med deltaRow og deltaCol

    assertEquals(shiftedOnce.cp.row() - tetromino.cp.row(), deltaRow); //tester om det forskyvde objektet er forskjøvet riktig antall rader
    assertEquals(shiftedOnce.cp.col() - tetromino.cp.col(), deltaCol); //tester om det forskyvde objektet er forskjøvet riktig antall kolonner
    assertEquals(shiftedTwice.cp.row() - shiftedOnce.cp.row(), deltaRow); //tester om det andre forskyvde objektet er forskjøvet riktig antall rader i forhold til det første forskyvde objektet
    assertEquals(shiftedTwice.cp.col() - shiftedOnce.cp.col(), deltaCol); ////tester om det andre forskyvde objektet er forskjøvet riktig antall kolonner i forhold til det første forskyvde objektet
    assertEquals(shiftedTwice.cp.row() - tetromino.cp.row(), deltaRow * 2); //tester om det andre forskyvde objektet er forskjøvet riktig antall rader i forhold til det opprinnelige Tetromino-objektet, altså ganger 2
    assertEquals(shiftedTwice.cp.col() - tetromino.cp.col(), deltaCol * 2); //tester om det andre forskyvde objektet er forskjøvet riktig antall kolonner i forhold til det opprinnelige Tetromino-objektet, altså ganger 2
}



@Test
public void testShiftedToTopCenterOf() {
    // Test med en 4x4 tetromino jevnt antall kolonner
    Tetromino tetromino1 = Tetromino.newTetromino('I');
    tetromino1 = tetromino1.shiftedToTopCenterOf(new TetrisBoard(4, 8));
    assertEquals(tetromino1.cp.row(), -1); //alle brikkene starter i row -1, siden alle har "tomme" celler øverst
    assertEquals(tetromino1.cp.col(), 2);

    // Test med en 4x4 tetromino og et ujevnt antall kolonner
    Tetromino tetromino2 = Tetromino.newTetromino('O'); //O er 4*4 selv om man bare kan se 2*2, må derfor regner med de "tomme" cellene som lager O
    tetromino2 = tetromino2.shiftedToTopCenterOf(new TetrisBoard(4, 7));
    assertEquals(tetromino2.cp.row(), -1);
    assertEquals(tetromino2.cp.col(), 2); //O starter egentlig i kolonne 3, men den har "tomme" celler på hver side så egentlig er den i 2

    // Test med en 3x3 tetromino og et jevnt antall kolonner
    Tetromino tetromino3 = Tetromino.newTetromino('J');
    tetromino3 = tetromino3.shiftedToTopCenterOf(new TetrisBoard(5, 8));
    assertEquals(tetromino3.cp.row(), -1);
    assertEquals(tetromino3.cp.col(), 2);

    // Test med en 3x3 tetromino og et ujevnt antall kolonner
    Tetromino tetromino4 = Tetromino.newTetromino('T');
    tetromino4 = tetromino4.shiftedToTopCenterOf(new TetrisBoard(5, 7));
    assertEquals(tetromino4.cp.row(), -1);
    assertEquals(tetromino4.cp.col(), 2);
}




@Test
public void testrotateCopyTetromino() { //sjekker rotasjon for T brikken
  Tetromino tetro1 = Tetromino.newTetromino('T');
  Tetromino tetro2 = Tetromino.newTetromino('T');
  Tetromino tetro3 = Tetromino.newTetromino('T');

  tetro1 = tetro1.rotateCopyTetromino();
  tetro1 = tetro1.rotateCopyTetromino();
  tetro1 = tetro1.rotateCopyTetromino();
  tetro1 = tetro1.rotateCopyTetromino(); //roter 1 helt rundt

  tetro3 = tetro3.rotateCopyTetromino();  //roterer 3 en gang

  assertEquals(tetro1, tetro2); //sjekker at de er like

  assertNotEquals(tetro1, tetro3); //sjekker at de ikke er like




}


    
}
