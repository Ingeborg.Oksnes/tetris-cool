package no.uib.inf101.tetris.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import org.junit.jupiter.api.Test;

import no.uib.inf101.grid.CellPosition;


public class TestTetrisBoard {

@Test
public void testPrettyString() {
  TetrisBoard board = new TetrisBoard(3, 4);
  board.set(new CellPosition(0, 0), 'g');
  board.set(new CellPosition(0, 3), 'y');
  board.set(new CellPosition(2, 0), 'r');
  board.set(new CellPosition(2, 3), 'b');
  String expected = String.join("\n", new String[] {
      "g--y",
      "----",
      "r--b"
  });
  assertEquals(expected, board.prettyString());
}



@Test
public void testRemoveFullRows() {
  // Tester at fulle rader fjernes i brettet:


  TetrisBoard board = new TetrisBoard(5, 2);
  board.set(new CellPosition(0, 1), 'T');
  board.set(new CellPosition(1, 0), 'T');
  board.set(new CellPosition(1, 1), 'T');
  board.set(new CellPosition(2, 1), 'T');
  board.set(new CellPosition(4, 0), 'L');
  board.set(new CellPosition(4, 1), 'L');
  board.set(new CellPosition(3, 0), 'L');
  board.set(new CellPosition(2, 0), 'L');

  assertEquals(3, board.removeRows());

  String expected = String.join("\n", new String[] {
    "--",
    "--",
    "--",
    "-T",
    "L-"
  });
  assertEquals(expected, board.prettyString());
}


@Test
public void testRemoveRows() { //egenlaget test
  TetrisBoard board = new TetrisBoard(10,3); //lager et brett med 3 kolonner
  

  board.set(new CellPosition(9, 0), 'O'); //fyller alle kolonnene i nederste raden
  board.set(new CellPosition(9, 1), 'O');
  board.set(new CellPosition(9, 2), 'O');

  board.removeRows(); //kaller på removerows


  assertFalse(board.get(new CellPosition(9,0))=='O'); //sjekker at de ikke lenger ligger i raden
  assertFalse(board.get(new CellPosition(9,1))=='O');
  assertFalse(board.get(new CellPosition(9,2))=='O');
  }
    
}
