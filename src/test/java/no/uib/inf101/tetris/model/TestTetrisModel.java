package no.uib.inf101.tetris.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import no.uib.inf101.grid.CellPosition;
import no.uib.inf101.grid.GridCell;
import no.uib.inf101.tetris.model.tetromino.PatternedTetrominoFactory;
import no.uib.inf101.tetris.model.tetromino.TetrominoFactory;
import no.uib.inf101.tetris.view.ViewableTetrisModel;

public class TestTetrisModel {

@Test
public void initialPositionOfO() {
  TetrisBoard board = new TetrisBoard(20, 10);
  TetrominoFactory tetrominoFactor = new PatternedTetrominoFactory("O");
  ViewableTetrisModel fallingBrick = new TetrisModel(board, tetrominoFactor);

  List<GridCell<Character>> tetroCells = new ArrayList<>();
  for (GridCell<Character> gc : fallingBrick.availableCells()) {
    tetroCells.add(gc);
  }

  assertEquals(4, tetroCells.size());
  assertTrue(tetroCells.contains(new GridCell<>(new CellPosition(0, 4), 'O')));
  assertTrue(tetroCells.contains(new GridCell<>(new CellPosition(0, 5), 'O')));
  assertTrue(tetroCells.contains(new GridCell<>(new CellPosition(1, 4), 'O')));
  assertTrue(tetroCells.contains(new GridCell<>(new CellPosition(1, 5), 'O')));
}


@Test
public void initialPositionOfI() {
  TetrisBoard board = new TetrisBoard(20, 10); // oppretter et Tetris-brett med en bredde på 10 og en høyde på 20.
  TetrominoFactory factory = new PatternedTetrominoFactory("I"); //oppretter en fabrikk for Tetrominoer med typen "I".
  ViewableTetrisModel model = new TetrisModel(board, factory); //oppretter en modell av Tetris-spillet ved å bruke brettet og fabrikken vi nettopp opprettet.

  List<GridCell<Character>> tetroCells = new ArrayList<>(); //oppretter en tom liste som skal holde på cellene som tetrominoen består av
  for (GridCell<Character> gc : model.availableCells()) { //går vi gjennom alle cellene som er tilgjengelige i modellen 
    tetroCells.add(gc); //og legger dem til i listen som vi nettopp opprettet
  }

  assertEquals(4, tetroCells.size()); //sjekker at antall celler som tetrominoen består av er 4.
  assertTrue(tetroCells.contains(new GridCell<>(new CellPosition(0, 3), 'I'))); //sjekker at det er en celle i listen som har posisjon (0,3) og som inneholder bokstaven 'I'
  assertTrue(tetroCells.contains(new GridCell<>(new CellPosition(0, 4), 'I'))); //sjekker at det er en celle i listen som har posisjon (0,4) og som inneholder bokstaven 'I'
  assertTrue(tetroCells.contains(new GridCell<>(new CellPosition(0, 5), 'I'))); //sjekker at det er en celle i listen som har posisjon (0,5) og som inneholder bokstaven 'I'
  assertTrue(tetroCells.contains(new GridCell<>(new CellPosition(0, 6), 'I'))); //sjekker at det er en celle i listen som har posisjon (0,6) og som inneholder bokstaven 'I'
}


@Test
public void legalRotateTetromino() { //sjekker om rotasjonen er lovlig
  TetrisBoard board = new TetrisBoard(10,10);
  TetrominoFactory tetrominoFactor = new PatternedTetrominoFactory("S");
  TetrisModel model = new TetrisModel(board, tetrominoFactor);

  model.moveTetromino(9,1); //flytter tetrominoen til nederste rad, og skal da ikke kunne rotere brikken herifra
  assertFalse(model.rotateTetromino()); //sjekker at metoden returnerer false

  model.moveTetromino(3,0); //flytter brikken tre rader ned
  assertTrue(model.rotateTetromino()); //da skal det være mulig og rotere og den skal returnere true
} 

    
@Test
public void testMoveTetrominoSuccessful() { //tester om om vellykket flytting returnerer true
TetrisBoard board = new TetrisBoard(4, 10);
TetrominoFactory factory = new PatternedTetrominoFactory("I");
TetrisModel model = new TetrisModel(board, factory);

boolean result = model.moveTetromino(1, 0); //skal flytte tetrominoen med en rad
assertTrue(result);
boolean result2 = model.moveTetromino(1, 0); //skal flytte tetrominoen med en rad
assertTrue(result2);
boolean result3 = model.moveTetromino(1, 0); //skal flytte tetrominoen med en rad
assertTrue(result3);
boolean result4 = model.moveTetromino(1, 0); //skal flytte tetrominoen med en rad
assertFalse(result4); //her skal den være flyttet ut av raden og derfor bli false

}


@Test
public void testMoveTetrominoOutOfBounds() { //sjekker at vi ikke kan flytte brikken ut av brettet med moveTetromino
  TetrisBoard board = new TetrisBoard(5, 5);
  TetrominoFactory factory = new PatternedTetrominoFactory("T");
  TetrisModel model = new TetrisModel(board, factory);

  boolean result = model.moveTetromino(4, 0); //flytter tetrominoen ut av brettet nedover
  assertFalse(result);
  boolean result1 = model.moveTetromino(-1, 0); //flytter tetrominoen ut av brettet oppover
  assertFalse(result1);
  boolean result2 = model.moveTetromino(0, 2); //flytter tetrominoen ut av brettet til høyre
  assertFalse(result2);
  boolean result3 = model.moveTetromino(0, -2); //flytter tetrominoen ut av brettet til venstre
  assertFalse(result3);


}

@Test
public void testMoveTetrominoIllegal() {
  TetrisBoard board = new TetrisBoard(10, 10);
  TetrominoFactory factory = new PatternedTetrominoFactory("L");
  TetrisModel model = new TetrisModel(board, factory);
 


  board.set(new CellPosition(5, 5), 'O'); //setter en O på brettet
  

  boolean result = model.moveTetromino(5, 0); //flytter tetrominoen til der det allerede er en brikke
  assertFalse(result);
  boolean result1 = model.moveTetromino(6, 6); //flytter tetrominoen til der det allerede er en brikke
  assertFalse(result1);


  board.set(new CellPosition(9, 0), 'I'); //flytter en brikke helt nederst i hjørne
  boolean result2 = model.moveTetromino(9, -4); //flytter tetrominoen til der det allerede er en brikke
  assertFalse(result2);


  }


@Test
public void testDropTetromino() {
  TetrisBoard board = new TetrisBoard(10,10);
  TetrominoFactory tetrominoFactor = new PatternedTetrominoFactory("I");
  TetrisModel model = new TetrisModel(board, tetrominoFactor);

 
  model.dropTetromino(); //sender den til bunnen
  List<GridCell<Character>> tetroCells = new ArrayList<>(); //oppretter en tom liste som skal holde på cellene som tetrominoen består av
  for (GridCell<Character> gc : model.getTilesOnBoard()) { //går vi gjennom alle cellene 
    tetroCells.add(gc); //og legger dem til i listen som vi nettopp opprettet
  }

 
 //sjekker når at tetrominoen har flyttet seg en rad ned, ved å sjekke at cellene er dekt
  assertTrue(tetroCells.contains(new GridCell<>(new CellPosition(9, 3), 'I'))); 
  assertTrue(tetroCells.contains(new GridCell<>(new CellPosition(9, 4), 'I'))); 
  assertTrue(tetroCells.contains(new GridCell<>(new CellPosition(9, 5), 'I'))); 
  assertTrue(tetroCells.contains(new GridCell<>(new CellPosition(9, 6), 'I')));
}





@Test
public void testClockTick() {
  TetrisBoard board = new TetrisBoard(10,10);
  TetrominoFactory tetrominoFactor = new PatternedTetrominoFactory("I");
  TetrisModel model = new TetrisModel(board, tetrominoFactor);

 
  model.clockTick(); //kjører clockTick en gang
  List<GridCell<Character>> tetroCells = new ArrayList<>(); //oppretter en tom liste som skal holde på cellene som tetrominoen består av
  for (GridCell<Character> gc : model.availableCells()) { //går vi gjennom alle cellene
    tetroCells.add(gc); //og legger dem til i listen som vi nettopp opprettet
  }

 
 //sjekker at tetrominoen har flyttet seg en rad ned etter ett tikk, ved å sjekke at cellene er dekt
  assertTrue(tetroCells.contains(new GridCell<>(new CellPosition(1, 3), 'I'))); 
  assertTrue(tetroCells.contains(new GridCell<>(new CellPosition(1, 4), 'I'))); 
  assertTrue(tetroCells.contains(new GridCell<>(new CellPosition(1, 5), 'I'))); 
  assertTrue(tetroCells.contains(new GridCell<>(new CellPosition(1, 6), 'I')));

}

}
