package no.uib.inf101.tetris.model.tetromino;

public class PatternedTetrominoFactory implements TetrominoFactory {
    String string;
    int counter = 0;

    public PatternedTetrominoFactory(String string){
        this.string = string;

        
    }


    @Override
    public Tetromino getNext() { // brukes til å hente den neste brikken som spilleren skal plassere på spillefeltet
        char letter = string.charAt(counter); //strengen inneholder en serie med bokstaver som representerer forskjellige Tetris-brikker
        counter++; //øker counter med 1
        counter %= string.length(); //Hvis telleren har nådd slutten av strengen, settes den tilbake til 0, slik at neste bokstav kan hentes fra begynnelsen av strengen
        return Tetromino.newTetromino(letter); //oppretter en ny Tetromino-objekt, bokstaven som ble hentet fra strengen brukes til å bestemme hvilken type Tetris-brikke som skal opprettes.

         /*Character letter = string.charAt(counter);
        counter = (counter+1) % string.length();
        return Tetromino.newTetromino(letter);*/
        
        

    }
    
}
