package no.uib.inf101.tetris.model.tetromino;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;


import no.uib.inf101.grid.CellPosition;
import no.uib.inf101.grid.GridCell;
import no.uib.inf101.grid.GridDimension;


public final class Tetromino implements Iterable<GridCell<Character>> {  //final fordi klassen er uforanderlig, objekt skal ikke kunne muteres
    char symbol; //reprsenterer symbolet til brikken
    boolean [][] shape; //fasongen til brikken
    CellPosition cp; //posisjonen til hjørnet øverst til venstre på brikken

    
    public Tetromino(char character, boolean[][] shape, CellPosition cp) { //gjør den private så det ikke skal være mulig å konstruere hvilke som helst Tetromino-objekter
        this.symbol = character;
        this.shape = shape;
        this.cp = cp;

    }


    static Tetromino newTetromino(char character) { //pakke privat
        CellPosition cp = new CellPosition(0, 0);
        boolean[][] shape = switch(character) {
            case 'L' -> new boolean[][] {  //lager cases for alle mulige brikker, ut ifra mal fra oppgavetekst
                    { false, false, false },
                    {  true,  true,  true },
                    { true,  false, false }
            };
            case 'J' -> new boolean[][] {
                { false, false, false },
                {  true,  true,  true },
                { false,  false, true }
            };
            case 'S' -> new boolean[][] {
                { false, false, false },
                {  false,  true,  true },
                { true,  true, false }
            };
            case 'Z' -> new boolean[][] {
                { false, false, false },
                {  true,  true,  false },
                { false,  true, true }
            };
            case 'T' -> new boolean[][] {
                { false, false, false },
                {  true,  true,  true },
                { false,  true, false }
            };
            case 'I' -> new boolean[][] {
                { false, false, false, false },
                {  true,  true,  true, true },
                { false,  false, false, false },
                { false,  false, false, false }
            };
            case 'O' -> new boolean[][] {
                { false, false, false, false },
                {  false,  true,  true, false },
                { false,  true, true, false },
                { false,  false, false, false }
            };

            default -> throw new IllegalArgumentException(
        "No available shape for '" + character + "'");
        };
    

          return new Tetromino(character, shape, cp);
    }
        


    public Tetromino shiftedBy(int deltaRow, int deltaCol) { //en metode for å flytte ne tetromino uten å endre det opprinnelige objektet
        CellPosition cellPosition = new CellPosition((this.cp.row() + deltaRow), (this.cp.col() + deltaCol)); //oppretter en ny cellposition som representerer den nye posisjonen til tetromino
        return new Tetromino(symbol, shape, cellPosition); //returnerer et nytt tetrominoobjekt, men med samme symbol og shape som den opprinnelige men med ny posisjon

    }


    public Tetromino shiftedToTopCenterOf(GridDimension gridDimension) { //metode for å sette tertominoen til øverste rad og sentrum av spillefeltet, slik at det er klart til å falle nedover.


        int newRow = -1; //ønsker å flytte terominoen til øverste rad, siden øverste rad alltid er "false" eller tom, skal den plasseres på -1
        int newCol = gridDimension.cols() / 2 - 2 + gridDimension.cols() % 2; //beregner den nye kolonneverdien
        //int newCol = gridDimension.cols() / 2 - 1 + gridDimension.cols() % 2;
        

        return new Tetromino(symbol, shape, new CellPosition(newRow, newCol)); //returnerer en ny tetromino som er lik den opprinnelige men med ny cellposition
    
        

        
    }


    @Override
    public Iterator<GridCell<Character>> iterator() {
        List<GridCell<Character>> gridCells = new ArrayList<>(); //gridCells vil inneholde alle de opptatte cellene      
        for (int row = 0; row < shape.length; row++) {
            for (int col = 0; col < shape[0].length; col++) { //gå gjennom alle cellene i shape-arrayet til Tetromino-objektet
                if (shape[row][col]) { //For hver celle i shape-arrayet sjekker if-setningen om denne cellen er opptatt av Tetromino
                        gridCells.add(new GridCell<Character>(new CellPosition(cp.row() + row, cp.col() + col), symbol)); //Hvis cellen er opptatt, legger koden til en ny GridCell<Character> med cellposition og symbol i gridCells
                }
            }
        }
        
        return gridCells.iterator(); //returnerer en iterator for gridCells
    }
        
    


    @Override
    public int hashCode() { //metode som beregner en hashverdi for tetromino
        return Objects.hash(this.symbol, Arrays.deepHashCode(this.shape), this.cp); //brukte eksempelet fra oppgaveteksten
    }



    @Override
    public boolean equals(Object obj) { //skal sjekke om to objekter er like eller ikke
        if (this == obj) { //sjekker om de to referansene peker på det samme objektet
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) { //hvis objekter ikke er null eller hvis klassen til objektene er ulike
            return false;
        }
        Tetromino other = (Tetromino) obj; //konverterer "obj" som er gitt som parameter i metoden til en Tetromino (da denne kan være hvilken som helst type objekt)
        return symbol == other.symbol && Arrays.deepEquals(shape, other.shape) && cp.equals(other.cp); //sjekker om symbolene i to Tetromino-objekter er like, sjekker om formene (shapes) til to Tetromino-objekter er like, sjekker om posisjonene (cp) til to Tetromino-objekter er like
        
        

    }
  
  
    public Tetromino rotateCopyTetromino() {
        boolean[][] rotatedTetromino = new boolean[shape.length][shape[0].length]; //oppretter en ny 2-dimensjonal boolean array som representerer den roterte tetrominoen

        int rows = rotatedTetromino.length; //antall raderxs i den roterte arrayen
        int cols = rotatedTetromino[0].length; //antall kolonner i den roterte arrayen

        for (int i = 0; i < rows; i++) { //bruker en nested løkke for å kopiere hver verdi fra den opprinnelige tetrominoen til den roterte arrayen
            for (int j = 0; j < cols; j++) {
                rotatedTetromino[i][j] = shape[j][rows - 1 - i]; //for hver posisjon i den roterte arrayen, henter vi verdien fra den opprinnelige arrayen som ligger 90 grader mot klokken
            }
        }
        return new Tetromino(symbol,rotatedTetromino, cp); //oppretter en ny tetromino med samme symbol og cellposition som den orginale, men med den roterte arrayen

        } 
        
}




    

