package no.uib.inf101.tetris.model;

import no.uib.inf101.grid.GridCell;
import no.uib.inf101.grid.GridDimension;
import no.uib.inf101.tetris.controller.ControllableTetrisModel;
import no.uib.inf101.tetris.model.tetromino.Tetromino;
import no.uib.inf101.tetris.model.tetromino.TetrominoFactory;
import no.uib.inf101.tetris.view.ViewableTetrisModel;

public class TetrisModel implements ViewableTetrisModel, ControllableTetrisModel {
    //feltvariabler
    TetrisBoard board;
    TetrominoFactory tetrominoFactor;
    Tetromino fallingBrick;
    GameState gameState;
    int score;
    

    public TetrisModel(TetrisBoard board, TetrominoFactory tetrominoFactor) { //konstruktør
        this.board = board;
        this.tetrominoFactor = tetrominoFactor;
        fallingBrick = tetrominoFactor.getNext().shiftedToTopCenterOf(board);
        gameState = GameState.ACTIVE_GAME;
       
        
    }



    @Override
    public GridDimension getDimension() {
        return board;
    }


    @Override
    public Iterable<GridCell<Character>> getTilesOnBoard() {
        return board; 
    }


    @Override
    public Iterable<GridCell<Character>> availableCells() {
        return fallingBrick;
    }


    @Override
    public boolean moveTetromino(int deltaRow, int deltaCol) { //prøver å flytte den nåværende tetrominoen en gitt avstand 
        Tetromino fallingBrickCopy = fallingBrick.shiftedBy(deltaRow, deltaCol); //oppretter en kopi av tetrominoen som har blitt flyttet
        if(isLegal(fallingBrickCopy) == true) { //sjekker om den flyttede tetrominoen er lovlig ved å kalle isLegal-metoden
            this.fallingBrick = fallingBrickCopy; //hvis den flyttede tetrominoen er lovlig, setter metoden den nåværende tetrominoen til å være kopi av den flyttede tetrominoen
            return true;
        }
        if(deltaRow == 1){ //hvis den treffer en anne brikke
            gluedTetromino(); // så skal den fallende brikken lime seg fast
            getNewFallingTetromino(); //og det skal spawne en new falling tetromino
        
        }
        return false;
         //returnerer false hvis den flyttede tetrominoen ikke er lovlig
    }
    
    

    private boolean isLegal(Tetromino tetromino) { //sjekker om en gitt tetromino er lovlig å plassere på spillebrettet
    
        for (GridCell<Character> cell : tetromino) { //for hver celle i tetrominoen 
            //CellPosition cellPos = cell.pos();
            if (!board.positionIsOnGrid(cell.pos()) || board.get(cell.pos()) != '-') { //sjekker om posisjonen til cellen er på spillebrettet og om cellen er ledig
                return false;
            }
        }
        return true;
    }
        


    @Override
    public boolean rotateTetromino() { //sjekker om rotasjonene er lovlig eller ikke
        Tetromino newTetromino = this.fallingBrick.rotateCopyTetromino(); //lager ett nytt tetromino objekt som er rotert 90 grader
        if(isLegal(newTetromino)) { //bruker is Legal metoden for å sjekke at den nye tetrominoen er lovlig plassert
            this.fallingBrick = newTetromino; //hvis den er lovlig oppdaterer vi den nåværende tetrominoen til den roterte tetrominoen
            return true; //også returnerer true
        }
        return false; //returnerer false hvis den ikke er lovlig og rotasjonen var ikke vellykket
    }


    @Override
    public boolean dropTetromino() { //slipper en fallende tetromino ned på spillebrettet så langt som den kan gå
        while (isLegal(this.fallingBrick.shiftedBy(1, 0))) { //beveger tetrominoen nedover spillebrettet så lenge dette er en lovlig bevegelse
            moveTetromino(1, 0); //bruker moveTetromino for å flytte brikken nedover brettet
        }
        gluedTetromino(); //kaller "gluedTetromino()" for å sette tetrominoen på spillebrettet og sjekke om spillet er over.
        board.removeRows(); //kaller "removeRows()" på spillebrettet for å fjerne fullstendige rader 
        getNewFallingTetromino(); //kaller "getNewFallingTetromino()" for å opprette en ny tetromino som skal falle ned på spillebrettet
        return true; //returnerer "true" for å indikere at handlingen var vellykket
    }


    private void getNewFallingTetromino() { //hjelpemetode for dropTetromino

        fallingBrick = tetrominoFactor.getNext().shiftedToTopCenterOf(board); //henter ut en ny brikke fra tetromino-fabrikken og oppdatere feltvariabelen for fallende brikke i modellen


        if(!isLegal(fallingBrick)) { //hvis brikken ikke er plassert lovlig på brettet
            gameState = GameState.GAME_OVER;  // endre gameStaten til game over
        }
        

    }


    private void gluedTetromino() { //setter de fallende tetrominoens celler på spillebrettet

        for (GridCell<Character> gridCell : fallingBrick) { //For hver celle i den fallende tetrominoen
            board.set(gridCell.pos(), gridCell.value()); //posisjonen og verdien til cellen blir satt i tilsvarende celle på spillebrettet ved å bruke metoden "set()" på board-objektet
            if (gridCell.pos().row() == 0) { //sjekker om tetrominoen har kollidert med toppen av spillebrettet
                gameState = GameState.GAME_OVER; //Hvis den har nådd toppen blir det game over
            }             
        }      

    }


    @Override
    public GameState getGameState() { //returnerer GameState-verdien til spillet

        return this.gameState;
    }


    public int milliSeconds() {
        return 1000; //1000 milliseconds = 1 sekund mellom hver gang tetrominoen faller
    }


    @Override
    public void clockTick() { //oppdaterer spillet ved å bevege tetrominoene nedover
        
        if (!(isLegal(this.fallingBrick))) { //hvis tetrominoen ikke er i en gyldig posisjon
            gluedTetromino(); //vil feste tetrominoet til spillbrettet og generere et nytt tetromino som faller fra toppen av brettet
         }
        else {
            moveTetromino(1, 0); //hvis tetrominoet er i en gyldig posisjon vil den flyttes en rad nedover
            
        }
    }


   
    
}
    


