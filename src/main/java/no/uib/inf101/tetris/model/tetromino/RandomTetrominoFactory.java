package no.uib.inf101.tetris.model.tetromino;

import java.util.Random;

public class RandomTetrominoFactory implements TetrominoFactory {
    private final char[] shape = {'L','J','S','Z','T','I','O'};
    private final Random random = new Random();


    @Override
        public Tetromino getNext() {
            char randomTetromino = shape[random.nextInt(shape.length)]; //velger ut en tilfeldig bokstav fra shape
            return Tetromino.newTetromino(randomTetromino); //returnerer en ny tilfeldig tetromino ved hjelp av en annen metode           
            
        }
    
}
