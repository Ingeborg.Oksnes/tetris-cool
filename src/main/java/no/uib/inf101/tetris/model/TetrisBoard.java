package no.uib.inf101.tetris.model;


import no.uib.inf101.grid.CellPosition;
import no.uib.inf101.grid.Grid;
import no.uib.inf101.grid.GridCell;

public class TetrisBoard extends Grid<Character> {  //denne klassen representerer et tetrisbrett, TetrisBoard-objekter er et Grid<Character>-objekter, og arver alle metodene i Grid-klassen.
    int rows; 
    int cols; 


    public TetrisBoard(int rows, int cols) { //rows = antall rader på brettet, cols = antall kolonner på brettet
        super(rows, cols, '-'); // '-' - verdien representerer at Tetris-brettet er tomt i den gitte posisjonen.

    }


    
    String prettyString() {  //lager en metode for å kunne teste at brettet blir riktig 
        String string = ""; //stringen skal ta inn de verdiene hver rute i raden på brettet har.
        for(GridCell<Character> cell : this ){ //itererer over hver celle
            string +=  cell.value(); // for hver rute legger vi til den verdien ruten har i den tomme stringen, henter ut verdien ved å hente ut value()
            if( cell.pos().col() >= cols()-1 && cell.pos().row() < rows()-1){ //sjekker om ruten vi er på er den siste i raden
                string+= "\n"; //hvis ruten vi er kommet til er den siste i raden, legger vi til et linjeskift
            }           
        }
        return string;  //returner strengen med alle verdiene fra rutene
    }


    private boolean elemetExistOnRow(int rows, Character value) { //sjekker om det finnes en celle i rutenettet med denne verdien og det angitte radnummeret
        for (GridCell<Character> gridCell : this) { //går gjennom hvert GridCell-objekt i rutenettet
            if (value == gridCell.value() && rows == gridCell.pos().row()) { //sjekker om det har riktig verdi og radnummer
                return true; //hvis en celle med denne kombinasjonen blir funnet, vil metoden returnere true
            }
        }
        return false; //hvis ikke, false
    }



    private void rowToPos(int rows, Character value) { //tar inn en radnummer "rows" og en verdi "value" og setter alle cellene i denne raden til å ha denne verdien
        for (int i = 0; i < this.cols(); i++) { //går gjennom hver kolonne i rutenettet
            set( new CellPosition(rows, i), value); //setter deretter verdien av cellen som tilsvarer den aktuelle raden og kolonnen til å være "value"

        }
    }


    private void copyRow(int oldRow, int newRow) { //kopierer en hel rad fra brettet og setter den inn på en annen rad

       for (int col = 0; col < this.cols(); col++) { //for hver kolonne i den gamle raden
            CellPosition oldCell = new CellPosition(oldRow, col); //oppretter en posisjon for den gamle cellen
            CellPosition newCell = new CellPosition(newRow, col); //oppretter en posisjon for den nye cellen
            char value = this.get(oldCell); //henter ut verdien til cellen i den gamle posisjonen
            this.set(newCell, value); //setter verdien til cellen i den nye posisjonen til å være den samme
        }
    }
        


    public int removeRows() { //fjerner alle rader på brettet som er helt fylt med verdier (ikke tomme eller null-verdier)
        int count = 0; //holder styr på antall fulle rader som fjernes
        int a = this.rows() - 1; //starter på nederste rad i brettet
        int b = this.rows() - 1; //starter på nederste rad i brettet

        while ( a >= 0) { //så lenge a er større eller lik null, altså så lenge den er innenfor radene i brettet, den vil altså gå fra bunnen og oppover
            while (b >= 0 && !elemetExistOnRow(b, '-')){ //så lenge b er større eller lik null, altså innenfor brettet og det ikke finnes tomme eller nullverdier i raden b
            count ++; //så lenge det ikke er en tom cell i raden, altså at det er en full rad, vil count gå en opp
            b --; //b blir minus en, altså at den flytter seg en rad opp
            }
            if( b>= 0) { //hvis b er større eller lik 0
                copyRow(b, a); //kopierer raden b til raden a
            }
            else {
                rowToPos(a, '-'); //hvis raden har tomme eller nullverdier, setter vi inn en ny rad med nullverdier i posisjon a
            }
            a--; //reduseres med 1 for å gå videre til neste rad i løkken
            b--; //reduseres med 1 for å gå videre til neste rad i løkken
        
        
    }
    return count; //returnerer count som er tellingen av antall rader som er fjernet
    }



}
