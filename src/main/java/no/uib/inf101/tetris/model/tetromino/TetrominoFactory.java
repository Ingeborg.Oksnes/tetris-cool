package no.uib.inf101.tetris.model.tetromino;

public interface TetrominoFactory {

    /**
     * @returns a new Tetromino
     */
    public Tetromino getNext();

    

    
}
