package no.uib.inf101.tetris.controller;
import java.awt.event.ActionEvent;

import java.awt.event.KeyEvent;

import javax.swing.Timer;

import no.uib.inf101.tetris.midi.TetrisSong;
import no.uib.inf101.tetris.model.GameState;
import no.uib.inf101.tetris.view.TetrisView;




public class TetrisController implements java.awt.event.KeyListener { 
    //feltvariabler
    ControllableTetrisModel tetrisGame;
    TetrisView tetrisView;
    TetrisSong tetrisSong;
    Timer timer;
  
    

    public TetrisController(ControllableTetrisModel tetrisGame, TetrisView tetrisView) { //konstruktør
        this.tetrisGame = tetrisGame;
        this.tetrisView = tetrisView;
        this.tetrisSong = new TetrisSong();

        tetrisView.setFocusable(true);
        tetrisView.addKeyListener(this);
        
        this.timer = new Timer(tetrisGame.milliSeconds(), this::clockTick); //eksempelet fra oppgaveteksten

        timer.start(); //får brikkene til å flytte seg nedover av seg selv
        tetrisSong.run(); //starter sangen


    }

    @Override
    public void keyTyped(KeyEvent e) {
        
    }


    @Override
    public void keyPressed(KeyEvent e) { //metode laget med malen fra oppgaveteksten
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            tetrisGame.moveTetromino(0,-1); //flytter minus en kolonne, altså ett steg til venstre
        }
        else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            tetrisGame.moveTetromino(0,1); //flytter pluss en kolonne, altså ett steg til venstre
        }
        else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            if (tetrisGame.getGameState() == GameState.ACTIVE_GAME ){ //kan kun bruke down 
            tetrisGame.moveTetromino(1,0); //flytter pluss 1 rad, altså ett hakk ned
            timer.restart();
            }
        }
        else if (e.getKeyCode() == KeyEvent.VK_UP) {
            tetrisGame.rotateTetromino(); //roterer brikken når vi trykker pil opp
       }
      else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            if (tetrisGame.getGameState() == GameState.ACTIVE_GAME ) { //kan kun bruke hvis gamestate er active
                tetrisGame.dropTetromino(); //dropper ned tetrominoen til bunnen
                timer.restart(); //lager koden smudere

            }
            

        } 
        
        tetrisView.repaint(); //sånn at visningen vet at den må tegne på nytt når vi gjør flytt
        } 
    
    



    @Override
    public void keyReleased(KeyEvent e) {

    }


    public void clockTick(ActionEvent actionEvent){
        if( tetrisGame.getGameState() == GameState.ACTIVE_GAME) { //hvis spillet fortsatt er i gang (det ikke er game over)
            tetrisGame.clockTick(); //kalles clockTick 
            tetrisView.repaint();
            

        }
        

    }

    private void getTimerDelay() { //hjelpefunskjon
        this.timer.setDelay(tetrisGame.milliSeconds());
        this.timer.setInitialDelay(tetrisGame.milliSeconds());

    }

 
     
}
