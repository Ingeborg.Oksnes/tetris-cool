package no.uib.inf101.tetris.controller;

import no.uib.inf101.tetris.model.GameState;


public interface ControllableTetrisModel {

    /**
     * @param deltaRow
     * @param deltaCol
     * @return true if the move actually hppend, false else
     */
    public boolean moveTetromino(int deltaRow, int deltaCol);



    /**
     * @returns true hvis rotasjonen er lovlig og oppdaterer brikken til en rotert kopi, false hvis den ikke er lovlig
     */
    public boolean rotateTetromino();



    /**
     * @return dropper tetrominoen nedover brettet så langt det er lov
     */
    public boolean dropTetromino();
    

    /**
     * @return et objekt av typen gamestate, enten game over eller active game
     */
    public GameState getGameState();


    /**
     * @return henter ut hvor mange millisekunder (som integer) det skal være mellom hvert klokkeslag
     */
    public int milliSeconds();


    /**
     * metoden som kalles hver gang klokken slår
     */
    public void clockTick();

    
}
