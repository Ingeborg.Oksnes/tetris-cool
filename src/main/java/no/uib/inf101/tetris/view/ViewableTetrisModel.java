package no.uib.inf101.tetris.view;

import no.uib.inf101.grid.GridCell;
import no.uib.inf101.grid.GridDimension;
import no.uib.inf101.tetris.model.GameState;

public interface ViewableTetrisModel {

    /**
     * 
     * @return returnerer et griddimension objekt
     */
    public GridDimension getDimension();


    /**
    * 
    * @return returner en Iterable<GridCell<Character>> som itererer over alle flisene på brettet, gir alle posisjonene på brettet med tilhørende symbol.
    */
    public Iterable<GridCell<Character>> getTilesOnBoard();



    /**
     * @return tilgjengeliggjør rutene til den fallende brikken
     */
    public Iterable<GridCell<Character>> availableCells();


    /**
     * @return gamestate, enten Game over eller active game
     */
    public GameState getGameState();
    
}
