package no.uib.inf101.tetris.view;
import java.awt.geom.Rectangle2D;

import no.uib.inf101.grid.CellPosition;
import no.uib.inf101.grid.GridDimension;

public class CellPositionToPixelConverter {
    Rectangle2D box; //beskriver innenfor hvilket område rutenettet skal tegnes
    GridDimension gd; //beskriver størrelsen til rutenettet rutene vil være en del av
    double margin; //beskriver hvor stor avstanden skal være mellom rutene


    public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) { //konstruktør
        this.box = box;
        this.gd = gd;
        this.margin = margin;
    }


    public Rectangle2D getBoundsForCell(CellPosition cellPosition) { //lab 4

        //oversetter koordinater i rutenettet til et rektangel med posisjon og størrelse beskrevet som piksler til bruk på et lerret.

        box.getX();  //henter ut variablene som representerer de ytre grensene til gridet
        box.getY();
        box.getHeight();
        box.getWidth();
        
        double colpos = cellPosition.col(); 
        double rowpos = cellPosition.row();

        double cellWidth = (box.getWidth() - ((this.gd.cols() + 1) * this.margin)) / this.gd.cols(); ////finner bredden og høyden til rektangelet for den gitte CellPosition
        double cellHeight = (box.getHeight() - ((this.gd.rows()+ 1) * this.margin)) / this.gd.rows();

        double cellX = box.getX() + this.margin * (colpos+1) + cellWidth * colpos; //finner posisjonen til rektangelet for den gitte CellPosition
        double cellY = box.getY() + this.margin * (rowpos + 1) + cellHeight * rowpos;
    
        Rectangle2D cell = new Rectangle2D.Double(cellX, cellY, cellWidth, cellHeight); //lager et rektangel med posisjonen og størrelsen vi har regnet ut

        return cell;

    }

}
