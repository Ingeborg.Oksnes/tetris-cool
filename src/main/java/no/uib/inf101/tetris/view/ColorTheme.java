package no.uib.inf101.tetris.view;

import java.awt.Color;


public interface ColorTheme {

   
    /**
     * @param c
     * @return kan ikke returnere null, skal returnere fargen på cellen
     */
    public Color getCellColor(char c);


    /**
     * @return new Color(0, 0, 0, 0), kan ikke være null, men kan returnere en gjennomsiktig farge, ramme-fargen
     */
    public Color getFrameColor();

    /**
     * @return kan være null, men fargen kan ikke være gjennomsiktig, bakgrunnsfargen
     */
    public Color getBackgroundColor();


    /**
     * @return gjennomsiktig farge til skjerm
     */
    public Color getGameColor();

    
}
