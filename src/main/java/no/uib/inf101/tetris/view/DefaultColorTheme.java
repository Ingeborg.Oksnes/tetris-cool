package no.uib.inf101.tetris.view;

import java.awt.Color;

public class DefaultColorTheme implements ColorTheme {


    @Override
    public Color getCellColor(char c) {  
        Color color = switch(c) { //brukte mal fra oppgaveteksten
            case 'L' -> Color.RED;  
            case 'T' -> Color.CYAN;
            case 'O' -> Color.ORANGE;
            case 'P' -> Color.BLUE;
            case 'S' -> Color.GREEN;
            case 'I' -> Color.YELLOW;
            case 'J' -> Color.PINK;
            case '-' -> Color.BLACK;
            case 'Z' -> Color.MAGENTA;
            default -> throw new IllegalArgumentException(
                "No available color for '" + c + "'");
                
          };
          return color;
        
    }


    @Override
    public Color getFrameColor() {
        
        return new Color(0, 0, 0, 0);
    
        
        
           
    }


    @Override
    public Color getBackgroundColor() {
        
        return null;
        
    }   

    
    public Color getGameColor() { //hvis spillet er game over, tegner vi en rute med gjennomsiktig farge
        return new Color(0, 0, 0, 128);
    }
    
}
