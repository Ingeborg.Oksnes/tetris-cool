package no.uib.inf101.tetris.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.geom.Rectangle2D;
import java.awt.Graphics2D;
import java.awt.Graphics;

import javax.swing.JPanel;

import no.uib.inf101.grid.GridCell;
import no.uib.inf101.tetris.model.GameState;



public class TetrisView extends JPanel {
    ViewableTetrisModel ViewableTetrisModel;
    ColorTheme colorTheme;

    private static final int OUTERMARGIN = 2;  
    private static final int INNERMARGIN = 2;
    private Color backgroundColor;


 


    public TetrisView(ViewableTetrisModel model) {
        this.ViewableTetrisModel = model;
        this.colorTheme = new DefaultColorTheme();

        Color backgroundColor = colorTheme.getBackgroundColor();
        if( backgroundColor != null) {
            this.setBackground(backgroundColor);
        }

        this.setPreferredSize(new Dimension(404, 604));    

    }


 


    private static void drawCells(Graphics2D graphics2D, Iterable<GridCell<Character>> cellCollection, CellPositionToPixelConverter cellPositionToPixelConverter, ColorTheme colorTheme){
   //metode som tegner cellene/rutene på brettet
    for (GridCell<Character> cell : cellCollection){ //itererer over alle rutene i cellCollection
       Rectangle2D rectangle = cellPositionToPixelConverter.getBoundsForCell(cell.pos()); //fro hver celle lages det e rektangel
       Color color = colorTheme.getCellColor(cell.value());
       graphics2D.setColor(color); //setColor brukes for å sette fargen på objektet til fargen på cellen
       graphics2D.fill(rectangle); //fyller rektangelet vi laget med den fargen


    }
}




    private void drawGame(Graphics2D graphics2D){ // lab 4
        //Graphics2D g2 = (Graphics2D) graphics2D;
        double x = OUTERMARGIN ; //disse 4 variablene brukes for å lage et rektangel som er "rammen" av spillet
        double y = INNERMARGIN ;
        double width = this.getWidth() - (2 * INNERMARGIN); 
        double height = this.getHeight() - (2 * OUTERMARGIN);

        Rectangle2D rektangel = new Rectangle2D.Double(x,y,width,height); //lager rektangelet med variablene
        graphics2D.setColor(colorTheme.getFrameColor()); //setter farge på rammen
        
        //graphics2D.setColor(backgroundColor);
        graphics2D.fill(rektangel); //fyller så rektangelet med framecolor

        CellPositionToPixelConverter cellPositionToPixelConverter = new CellPositionToPixelConverter(rektangel, ViewableTetrisModel.getDimension(), INNERMARGIN); //lager rutene 
        drawCells(graphics2D, ViewableTetrisModel.getTilesOnBoard(), cellPositionToPixelConverter, colorTheme); //cellPositionToPixelConverter konverterer posisjonen til hver celle til pixel koordinater
        drawCells(graphics2D, ViewableTetrisModel.availableCells(), cellPositionToPixelConverter, colorTheme);  //den fallende brikken tegnes «oppå» brettet vi allerede har tegnet

        if (ViewableTetrisModel.getGameState()== GameState.GAME_OVER) { //hvis vi har fått game over skal det tegnes en game over screen
            Rectangle2D gameOverScreen = new Rectangle2D.Double(0,0,this.getWidth() , this.getHeight()); //fyller hele rammen
            graphics2D.setColor(colorTheme.getGameColor());
            graphics2D.fill(gameOverScreen); //gjennomsiktig farge


            graphics2D.setColor(Color.RED);
            graphics2D.setFont(new Font("Ariel", Font.BOLD, 45));
            Inf101Graphics.drawCenteredString(graphics2D, "GAME OVER", this.getWidth()/2, this.getHeight()/2); //teksten mitt på skjermen

        }

        

        

        
    }

    public void paintComponent(Graphics g){
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        drawGame(g2);
    }


    
}
