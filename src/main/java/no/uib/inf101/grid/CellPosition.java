package no.uib.inf101.grid;

//brukt fra egen lab 4

/**
 * A CellPosition consists of a row and a column.
 *
 * @param row  the row of the cell
 * @param col  the column of the cell
 */
public record CellPosition(int row, int col) {}
