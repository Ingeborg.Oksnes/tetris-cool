package no.uib.inf101.grid;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

//brukt mye fra min egen lab4

public class Grid<E> implements IGrid<E> { 
	private int cols;
	private int rows;
	private List<List<E>> grid;

	public Grid (int rows, int cols) { //konstruktør 1
		this(rows, cols, null);
	}

	

	public Grid(int rows, int cols, E value) { //konstruktør 2
		this.cols = cols;
		this.rows = rows;
		this.grid = new ArrayList<>();


		

		for (int i = 0; i < rows; i++){

			grid.add(new ArrayList<>()); //legger til en ny liste i listen vår, så vi får liste i liste

			for (int j = 0; j < cols; j ++) {
				
				grid.get(i).add(value); //legger til value i hver liste i listen
			}
			
		}
	}

	@Override
	public int rows() {

		return grid.size();  //lengden på listen vår (antall rader)
	}


	@Override
	public int cols() {
	
		return grid.get(0).size(); //lengden på den første lista i listen, siden det er en grid er alle listene like lange (antall kolonner)
	}

	
	@Override
	public Iterator<GridCell<E>> iterator() {
		List<GridCell<E>> cells = new ArrayList<>(); //oppretter en tom liste som vil inneholde cellene i nettverket som skal returneres av iteratoren
		
		for (int row = 0; row < rows; row++) { //for hver rad
			for (int col = 0; col < cols; col++) { //for hver kolonne
				cells.add(new GridCell<>(new CellPosition(row, col), get(new CellPosition(row, col)))); //legger til en ny GridCell for hver rad-kolonne
			}
		}
		
		return cells.iterator(); //returnerer en iterator for listen
	}
		
	

	@Override
	public void set(CellPosition pos, E value) { //angi en verdi value på en bestemt posisjon pos i gridet

		grid.get(pos.row()).set(pos.col(), value);
		
	}



	@Override
	public E get(CellPosition pos) { //returnerer verdien på en bestemt posisjon pos i gridet
		return grid.get(pos.row()).get(pos.col());
	}


	@Override
	public boolean positionIsOnGrid(CellPosition pos) { //sjekker om en gitt posisjon er innenfor grensene til gridet, og returnerer true hvis posisjonen er på rutenettet og false ellers
		
		int row = pos.row();
		int col = pos.col();
	
		if (row >= 0 && row <  this.rows && col >= 0 && col < this.cols) { //hvis rad posisjon og kolonne posisjon er større enn null og mindre enn antallet rader og antallet kolonner i rutenettet
			return true;
		} else { 
			return false;
		}
		
	}



}
